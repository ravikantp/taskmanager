from django.contrib.auth.models import User
from django.db import models

# Create your models here.
class Task(models.Model):
    assignee = models.ForeignKey(User, null=True, on_delete=models.SET_NULL, related_name='assignee')
    creator = models.ForeignKey(User, null=True, on_delete=models.SET_NULL)

    task_title = models.CharField(max_length=200)
    description = models.CharField(null=True, max_length=5000)
    status = models.BooleanField(default=False)
    created = models.DateTimeField('date created', null=True)
    finished = models.DateTimeField('date finished', null=True)
    modified = models.DateTimeField('date modified', null=True)
