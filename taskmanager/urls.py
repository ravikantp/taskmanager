from django.urls import path
from django.conf.urls import url
from django.contrib.auth import views as auth_views

from . import views

app_name = 'taskmanager'
urlpatterns = [
    #ex: /taskmanager/
    path('', views.index, name='index'),

    path('login/', auth_views.LoginView.as_view( template_name = 'taskmanager/login.html'), name='login'),
    path('logout/', views.logout_view, name='logout'),
    path('register/', views.register, name='register'),
    # ex : /taskmanager/5/
    path('<int:task_id>/', views.detail, name='detail'),
    # ex: /taskmanager/completed_tasks
    path('completed_tasks/', views.completed_task_list, name='completed_tasks'),
    # /taskmanager/create
    path('create/',views.create, name='create'),
    # /taskmanager/5/edit
    path('<int:task_id>/edit/',views.edit, name='edit'),
    # /taskmanager/5/delete
    path('<int:task_id>/delete/',views.delete, name='delete'),
    # /taskmanager/5/toggle_status
    path('<int:task_id>/toggle_status/',views.toggle_status, name='toggle_status'),
]
