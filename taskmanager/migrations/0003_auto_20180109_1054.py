# Generated by Django 2.0 on 2018-01-09 10:54

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('taskmanager', '0002_auto_20180109_1015'),
    ]

    operations = [
        migrations.AlterField(
            model_name='task',
            name='description',
            field=models.CharField(max_length=5000, null=True),
        ),
        migrations.AlterField(
            model_name='task',
            name='finished',
            field=models.DateTimeField(null=True, verbose_name='date finished'),
        ),
    ]
