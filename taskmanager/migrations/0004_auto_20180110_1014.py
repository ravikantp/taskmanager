# Generated by Django 2.0 on 2018-01-10 10:14

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('taskmanager', '0003_auto_20180109_1054'),
    ]

    operations = [
        migrations.AddField(
            model_name='task',
            name='modified',
            field=models.DateTimeField(null=True, verbose_name='date finished'),
        ),
        migrations.AddField(
            model_name='task',
            name='status',
            field=models.BooleanField(default=True),
        ),
    ]
