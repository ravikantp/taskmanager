from django.http import HttpResponse, HttpResponseRedirect, Http404
from django.shortcuts import render, get_object_or_404
from django.urls import reverse
from django.utils import timezone
from django.contrib.auth.decorators import login_required
from django.contrib.auth import logout

from .models import Task
from .forms import TaskForm, UserForm


def logout_view(request):
    logout(request)
    return HttpResponseRedirect(reverse('taskmanager:index'))

def register(request):
    error_message = None
    if(request.method == 'POST'):
        form = UserForm(request.POST)
        if form.is_valid() and form.cleaned_data['password'] == form.cleaned_data['confirm_password']:
            user = form.save(commit=False)
            user.set_password(form.cleaned_data['password'])
            user.save()
            return HttpResponseRedirect(reverse('taskmanager:index'))
        elif form.cleaned_data['password'] != form.cleaned_data['confirm_password']:
            error_message = 'Passwords do not match!'
    else:
        form = UserForm()

    return render(request, 'taskmanager/register.html', {'form':form, 'error_message':error_message})

@login_required(login_url="login/")
def index(request):
    user = request.user
    latest_task_list = Task.objects.filter(assignee=user, status=False).order_by('-created')
    context = {'latest_task_list':latest_task_list}
    return render(request, 'taskmanager/index.html', context)

@login_required(login_url="login/")
def completed_task_list(request):
    user = request.user
    completed_task_list = Task.objects.filter(assignee=user, status=True).order_by('-finished')
    context = {'task_list':completed_task_list}
    return render(request, 'taskmanager/task_list.html', context)

@login_required(login_url="login/")
def create(request):
    user = request.user
    if(request.method == 'POST'):
        form = TaskForm(request.POST)
        if form.is_valid():
            task = form.save(commit=False)
            task.status = False
            task.assignee = user
            task.creator = user
            task.created = timezone.now()
            task.modified = timezone.now()
            task.save()
            return HttpResponseRedirect(reverse('taskmanager:detail', args=(task.id,)))
    else:
        form = TaskForm()

    return render(request, 'taskmanager/create_task.html', {'taskform':form})

@login_required(login_url="login/")
def edit(request, task_id):
    user = request.user
    if(request.method == 'POST'):
        form = TaskForm(request.POST)
        if form.is_valid():
            try:
                task = Task.objects.filter(assignee=user).get(pk=task_id)
            except Task.DoesNotExist:
                return render(request, 'taskmanager/edit_task.html', {'error_message':'Task does not exist'})
            else:
                task.task_title = form.cleaned_data['task_title']
                task.description = form.cleaned_data['description']
                task.modified = timezone.now()
                task.save()
                return HttpResponseRedirect(reverse('taskmanager:detail', args=(task.id,)))
    else:
        try:
            task = get_object_or_404(Task, pk=task_id)
        except(KeyError, Task.DoesNotExist):
            return render(request, 'taskmanager/edit_task.html', {'error_message':'Task does not exist'})
        else:
            form = TaskForm(instance=task)
            return render(request, 'taskmanager/edit_task.html', {'task':task, 'form': form})

@login_required(login_url="login/")
def delete(request, task_id):
    if(request.method == 'POST'):
        try:
            task = get_object_or_404(Task, pk=task_id)
        except(KeyError, Task.DoesNotExist):
            return render(request, 'taskmanager/delete.html', {'error_message':'Task does not exist'})
        else:
            task.delete()
            latest_task_list = Task.objects.order_by('-created')
            context = {'latest_task_list':latest_task_list}
            return render(request, 'taskmanager/index.html', context)
    else:
        return render(request, 'taskmanager/delete.html', {'error_message':'Get request not allowed!'})

@login_required(login_url="login/")
def detail(request, task_id):
    user = request.user
    try:
        task = Task.objects.filter(assignee=user).get(pk=task_id)
    except Task.DoesNotExist:
        raise Http404("Task does not exist OR you are unauthorized to see this task.")
    return render(request, 'taskmanager/detail.html', {'task':task})

@login_required(login_url="login/")
def toggle_status(request, task_id):
    user = request.user
    try:
        task = Task.objects.filter(assignee=user).get(pk=task_id)
    except task.DoesNotExist:
        raise Http404("Task does not exist.")
    else:
        task.status = not task.status
        if task.status == True:
            task.finished = timezone.now()
        task.save()
    return HttpResponseRedirect(reverse('taskmanager:detail', args=(task.id,)))
