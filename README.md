Pre-requisites : Django v2, Python v3+

For DB migrations:
    python manage.py makemigrations;
    python manage.py migrate

To run:
    python manage.py runserver
    
Open 127.0.0.1:8000/taskmanager/ to use this app.